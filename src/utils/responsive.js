import { Dimensions } from "react-native";
const {width, height} = Dimensions.get('window')


export const getPxPercentByHeight = (figmaPx) => {
  const screenHeight = 812
  const figmaPxToPercent = (figmaPx * 100 ) / screenHeight
  const divicePx = (height * figmaPxToPercent) / 100
  return Number.parseFloat(divicePx).toFixed(2)
}

export const getPxPercentByWidth = (figmaPx) => {
  const figmaWidth = 375
  const figmaPxToPercent = (figmaPx * 100 ) / figmaWidth
  const divicePx = (width * figmaPxToPercent) / 100
  return Number.parseFloat(divicePx).toFixed(2)
}
