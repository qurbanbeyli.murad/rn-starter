import React from 'react';
import PropTypes from 'prop-types';
import { View } from 'react-native';
import { useNavigation } from '@react-navigation/native';
import useTranslation from '../../../hooks/use-translation';
import {Button, Container, H1, Text, TextLink} from '../../commons';
import { InputText } from '../../forms';
import tw from '../../../../tailwind';
import AuthInput from "../../forms/AuthInput";
import Header from "../../general/Header";

export default function LoginForm({ onSubmit }) {
  const navigation = useNavigation();
  const { t } = useTranslation();

  return (
    <>
      <Header onPressLeftBtn={() => console.log('test')} title={'test'}/>
      <H1 style={tw('mb-6')}>{t('login.header')}</H1>

      <InputText keyboardType="email-address" />
      <InputText secureTextEntry />

      <AuthInput errorText={'public.phoneNumErr'}
                 maxLength={9} value={'phone'} label={'public.phoneNumber'} keyboardType={"phone-pad"}
        // onChange={(normalText) => onChangePhone(normalText)}
                 showValidation={false} placeholder={'000 00 00 000'}/>
      <View style={tw('my-6')}>
        <Button onPress={onSubmit} title={t('login.header')}/>
      </View>
      <Text>{t('login.paragraph')}</Text>
      <TextLink onPress={() => navigation.navigate('SignUp')}>
        {t('signup.header')}
      </TextLink>
    </>
  );
}

LoginForm.propTypes = {
  onSubmit: PropTypes.func,
};
