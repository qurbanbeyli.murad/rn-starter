import {StyleSheet} from 'react-native';
import {getPxPercentByHeight, getPxPercentByWidth} from "../../../utils/responsive";

export default StyleSheet.create({
    headerBox: {
        backgroundColor: '#fff',
        paddingHorizontal: Number(getPxPercentByWidth(20)),
        paddingVertical: Number(getPxPercentByHeight(18)),
        justifyContent: 'space-between',
        alignItems: 'center',
        borderBottomWidth: 1,
        borderColor: '#E0E3F0',
        flexDirection: 'row',
    },
    headerLeftBtn: {
        marginRight: Number(getPxPercentByWidth(20)),
        // backgroundColor: 'red',
        // padding: 5
    },
    headerLeft: {
        flexDirection: "row",
        alignItems: 'center'
    },
    headerTitle: {
        color: "#202027",
        // fontFamily: Inter_Medium,
        fontSize: Number(getPxPercentByHeight(16)),
        lineHeight: Number(getPxPercentByHeight(19)),
        fontWeight: '500'
    },
    headerRight: {
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
        // backgroundColor: 'red'
    },
    headerRightText: {
        color: '#3D73DF',
        // fontFamily: Inter_Medium,
        fontSize: Number(getPxPercentByHeight(12)),
        lineHeight: Number(getPxPercentByHeight(15)),
        fontWeight: '500'
    }

});
