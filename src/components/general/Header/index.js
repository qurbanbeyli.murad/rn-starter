import React from 'react';
import {Text, TouchableOpacity, View} from "react-native";
import style from "./index.style";
import SvgLeftArrow from "../../../assets/svgIncons/SvgLeftArrow";
import SvgCLose from "../../../assets/svgIncons/SvgClose";

function Header(props) {
    return (
        <View style={[style.headerBox, props.style]}>
            <View style={style.headerLeft}>
                { props?.onPressLeftBtn &&
                    <TouchableOpacity onPress={props?.onPressLeftBtn} style={style.headerLeftBtn}>
                        <SvgLeftArrow BackColor={"#202027"} />
                    </TouchableOpacity>
                }
                <Text style={style.headerTitle}>{props?.title}</Text>
            </View>
            <View style={style.headerRight}>
                { props?.onPressRightBtn &&
                <TouchableOpacity onPress={props?.onPressRightBtn} style={style.headerRightBtn}>
                    {props?.plus && <SvgLeftArrow BackColor={"#202027"}/>}
                    {props?.text && <Text style={style.headerRightText}>{props?.text}</Text>}
                    {props?.close && <SvgCLose fill={"#202027"}/>}
                </TouchableOpacity>
                }
            </View>
        </View>
    )
}

export default Header;
