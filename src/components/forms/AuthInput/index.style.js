import { StyleSheet } from 'react-native'
import {getPxPercentByHeight, getPxPercentByWidth} from "../../../utils/responsive";

export default StyleSheet.create({
  inputBox: {
    backgroundColor: "#fff",
    height: Number(getPxPercentByHeight(61)),
    borderWidth: 1,
    borderColor: 'rgba(224, 227, 240, 0.7)',
    borderRadius: 6,
    paddingTop: Number(getPxPercentByHeight(8)),
    marginTop: Number(getPxPercentByHeight(2)),
    width: '100%',
    justifyContent: 'flex-end',
  },
  myInput: {
    // backgroundColor: 'green',
    height: Number(getPxPercentByHeight(31)),
    paddingLeft: Number(getPxPercentByWidth(18)),
    borderBottomLeftRadius: 6,
    borderBottomRightRadius: 6,
    paddingTop: 0,
    fontSize:  Number(getPxPercentByHeight(14)),
    lineHeight:  Number(getPxPercentByHeight(17)),
    fontWeight: '500',

    // fontFamily: Inter_Medium,
    textAlignVertical: 'top'
    // width: '100%'
  },
  inputLabel: {
    fontSize:  Number(getPxPercentByHeight(12)),
    lineHeight:  Number(getPxPercentByHeight(13)),
    color: '#202027',
    marginLeft: Number(getPxPercentByWidth(15)),
    position: 'absolute',
    backgroundColor: '#fff',
    top: Number(getPxPercentByHeight(8)),
    paddingHorizontal: 3,
    borderRadius: 3,
    // fontFamily: Inter_Medium,
    fontWeight: '500',
  },
  inputErrorBox: {
    paddingTop: Number(getPxPercentByHeight( 10)),
    backgroundColor: '#fff',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    width: '100%'
  },
  errorMsg: {
    color: '#E04E4E',
    fontSize: Number(getPxPercentByHeight( 10)),
    lineHeight: Number(getPxPercentByHeight(16)),
    textAlign: 'right',
    // backgroundColor: 'green'
  },
  borderStyle: {
    width: '100%',
  },
  iconBtnBox: {
    position: 'absolute',
    top:  Number(getPxPercentByHeight( 22)),
    right: Number(getPxPercentByWidth(22)),
    backgroundColor: 'red'
  },
  placeholderStyle: {
    fontSize: 25
  }

})
