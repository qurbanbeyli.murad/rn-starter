import React, {useState} from 'react';
import {View, Text, TextInput} from "react-native";
import style from "./index.style";
import PropTypes from "prop-types";

const AuthInput = (props) => {
  const [focused, setFocused] = useState(false);
  const onFocus = () => {
    props?.onFocus
    setFocused(true)
  }
  const onBlur = () => {
    props?.onBlur
    setFocused(false)
  }
  return (
    <>
      {props?.showValidation ? <Text style={style.errorMsg}>{props?.errorText}</Text>: null}
      <View style={[style.inputBox, focused && {borderColor: props?.showValidation ? '#E04E4E' : '#3D73DF'}, props?.inputBoxStyle]}>
        <Text style={[style.inputLabel, focused ? {top: -6, color: props?.showValidation ? '#E04E4E' : '#3D73DF'} :
                    { color: props?.showValidation ? '#E04E4E' : '#202027'}]}>{props?.label || ""}</Text>
        <TextInput style={[style.myInput, props?.inputStyle, {color:props?.showValidation ? '#E04E4E' : '#202027'}]}
                   autoFocus={props?.autoFocus}
                   ref={props?.ref}
                   onFocus={onFocus}
                   multiline={props?.multiline || false}
                   secureTextEntry={props?.isSecureTextEntry}
                   placeholder={props?.placeholder || ""}
                   value={props?.value || ""}
                   keyboardType={props?.keyboardType}
                   onBlur={onBlur}
                   onChangeText={(text) => text.length <= props.maxLength ? props?.onChange(text) : null}
                   placeholderStyle={style.placeholderStyle}
                   placeholderTextColor='rgba(108, 110, 134, 0.6)'
                   autoCapitalize={props?.autoCapitalize} />
        {
          props?.iconR ? <View style={style.iconBtnBox}>{props?.iconR}</View> : null
        }
      </View>
    </>
  );
};

export default AuthInput;

AuthInput.propTypes = {
  value: PropTypes.string,
  errorText: PropTypes.string,
  label: PropTypes.string,
  placeholder: PropTypes.string,
  keyboardType: PropTypes.string,
  autoCapitalize: PropTypes.string,
  inputBoxStyle: PropTypes.object,
  ref: PropTypes.object,
  onPress: PropTypes.func,
  onFocus: PropTypes.func,
  onBlur: PropTypes.func,
  onChange: PropTypes.func,
  size: PropTypes.string,
  showValidation: PropTypes.bool,
  autoFocus: PropTypes.bool,
  multiline: PropTypes.bool,
  isSecureTextEntry: PropTypes.bool,
  iconR: PropTypes.node,

};
