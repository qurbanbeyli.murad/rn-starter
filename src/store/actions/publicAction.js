import {set_auth, set_auth_data, set_verify_for_register} from "../types/publicType";

export const setVerifyForRegister = (data) => {
  return async dispatch => {
    return dispatch({
      type: set_verify_for_register,
      payload: {
        isVerifyForRegister: data,
      }
    });
  };
};

export const setAuth = (data) => {
  return async dispatch => {
    return dispatch({
      type: set_auth,
      payload: {
        isAuth: data,
      }
    });
  };
};
