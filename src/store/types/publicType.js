export const set_auth = 'set_auth';
export const set_auth_data = 'set_auth_data';
export const set_verify_for_register = 'set_verify_for_register';
export const set_register_data = 'set_register_data';
