import AsyncStorage from '@react-native-async-storage/async-storage';
import {createStore, applyMiddleware} from 'redux';
import thunk from 'redux-thunk';
import {persistStore, persistReducer} from 'redux-persist';
import {composeWithDevTools} from 'redux-devtools-extension';
// import { createLogger } from 'redux-logger';

import rootReducer from './reducers';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  whitelist: ['publicReducer'],
  blacklist: [],
  timeout: 0,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const store = createStore(
  persistedReducer,
  composeWithDevTools(
    applyMiddleware(
      // createLogger(),
      thunk,
    ),
  ),
);

let persistor = persistStore(store);

export {store, persistor};
