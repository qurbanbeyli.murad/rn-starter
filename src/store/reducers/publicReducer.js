import { set_auth, set_verify_for_register, set_auth_data} from "../types/publicType";

const initialState = {
  isVerifyForRegister:false,
  isAuth:false,
};

const publicReducer = (state = initialState, action) => {
  switch (action.type) {
    case set_auth: {
      return {
        ...state,
        isAuth: action.payload.isAuth,
      }
    }
    case set_verify_for_register: {
      return {
        ...state,
        isVerifyForRegister: action.payload.isVerifyForRegister,
      }
    }
    default: {
      return state;
    }
  }
};

export default publicReducer;
