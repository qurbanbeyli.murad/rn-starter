import * as React from "react"
import Svg, { Path } from "react-native-svg"

function SvgLeftArrow(props) {
  return (
    <Svg
      width={props?.width || 29}
      height={props?.height || 16}
      viewBox="0 0 20 14"
      fill="none"
      xmlns="http://www.w3.org/2000/svg"
      {...props}
    >
      <Path
        d="M7.171 1L1 7m0 0l6.171 6M1 7h18"
        stroke="#202027"
        strokeWidth={2}
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </Svg>
  )
}

export default SvgLeftArrow
